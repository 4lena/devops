package com.ADF.springbootexample;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoanCalculatorController {

    @GetMapping("/")
    public String showLoanForm() {
        return "loan-form";
    }

    @PostMapping("/calculate")
    public String calculateLoan(@RequestParam double loanAmount,
                                @RequestParam double interestRate,
                                @RequestParam int loanTerm,
                                Model model) {
        double monthlyInterestRate = interestRate / 100 / 12;
        int numberOfPayments = loanTerm * 12;

        double monthlyPayment = (loanAmount * monthlyInterestRate) / (1 - Math.pow(1 + monthlyInterestRate, -numberOfPayments));

        model.addAttribute("monthlyPayment", monthlyPayment);
        return "loan-result";
    }
}
